#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.core.validators import EmailValidator
from django.core.exceptions import ValidationError


class Person(models.Model):
    """Человек."""
    last_name = models.CharField(
        'Фамилия',
        max_length=50,
    )
    first_name = models.CharField(
        'Имя',
        max_length=50,
    )
    middle_name = models.CharField(
        'Отчество',
        max_length=50,
        blank=True, null=True,
    )
    date_of_birth = models.DateField(
        'Дата рождения',
    )
    date_of_death = models.DateField(
        'Дата смерти',
        blank=True, null=True,
    )
    email = models.CharField(
        max_length=300,
        validators=[EmailValidator],
        blank=True, null=True,
        unique=True,
    )

    def __str__(self):
        result = [self.last_name, self.first_name]
        if self.middle_name:
            result.append(self.middle_name)
        return ' '.join(result)

    class Meta:
        verbose_name = 'Человек'
        verbose_name_plural = 'Человеки'

    def clean(self):
        super(Person, self).clean()

        if (self.date_of_birth and self.date_of_death and
                self.date_of_birth > self.date_of_death):
            raise ValidationError({
                'date_of_birth': 'Дата рождения не может быть позже даты смерти.'
            })
