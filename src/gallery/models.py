#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models


class Photo(models.Model):
    owner = models.ForeignKey(
        'auth.User'
    )
    url = models.URLField(
        'URL изображения',
    )
    comment = models.TextField(
        'Описание фотографии',
    )
    thumbnail = models.ImageField(
        blank=True,
        null=True,
        upload_to='photos/%Y/%m/%d'
    )

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'
        permissions = (
            ('view_own_photos', 'View only own photos'),
            ('view_all_photos', 'View all photos'),
        )
