#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import UpdateView
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

from django.core.urlresolvers import reverse

from gallery.models import Photo
from gallery.forms import PhotoForm
from itertools import islice


from gallery.tasks import thumb


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'index.html'
    context_object_name = 'photo_list'

    def dispatch(self, request, *args, **kwargs):
        has_perm = self.request.user.has_perm

        if (
            not has_perm('gallery.view_own_photos') and
            not has_perm('gallery.view_all_photos')
        ):
            return self.handle_no_permission()

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        has_perm = self.request.user.has_perm

        if has_perm('gallery.view_all_photos'):
            photos = Photo.objects.all()
        elif has_perm('gallery.view_own_photos'):
            photos = Photo.objects.filter(owner=self.request.user)
        else:
            raise PermissionDenied()

        context = super(TemplateView, self).get_context_data(**kwargs)
        rows = []
        photos = Photo.objects.iterator()
        while True:
            row = tuple(islice(photos, 3))
            rows.append(row)
            if len(row) < 3:
                break
        context.update({
            'rows': rows,
        })
        return context


class AddPhoto(PermissionRequiredMixin, CreateView):
    permission_required = 'gallery.add_photo'
    raise_exception = True
    model = Photo
    template_name = 'add.html'
    fields = ['url', 'comment']

    def form_valid(self, form):
        form.instance.owner_id = self.request.user.id
        return super().form_valid(form)

    def get_success_url(self):
        thumb(self.object.url, self.object.id, (256, 256))
        return reverse('index')


class DeletePhoto(PermissionRequiredMixin, DeleteView):
    permission_required = 'gallery.delete_photo'
    raise_exception = True
    model = Photo
    template_name = 'photo_check_delete.html'
    success_url = '/'


class EditPhoto(PermissionRequiredMixin, UpdateView):
    permission_required = 'gallery.change_photo'
    raise_exception = True
    template_name = 'edit.html'
    model = Photo
    form_class = PhotoForm
    success_url = '/'
