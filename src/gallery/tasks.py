from __future__ import absolute_import, unicode_literals
from celery import shared_task
import os
from django.core.files import File
from django.conf import settings
import requests
from io import BytesIO
from PIL import Image
from uuid import uuid4
from gallery.models import Photo


@shared_task
def download_photo(url, size):
    r = requests.get(url=url, stream=True)
    if r.status_code == 200:
        with BytesIO(r.content) as raw:
            with Image.open(raw) as photo:
                photo.thumbnail(size)
                photo_path = os.path.join(
                    settings.MEDIA_ROOT,
                    'temp',
                    str(uuid4()) + '.' + photo.format.lower()
                )
                photo.save(photo_path)
                return photo_path


@shared_task
def update_photo(photo_path, pk):
    try:
        photo = Photo.objects.get(pk=pk)
        file = File(open(photo_path, 'rb'))
        file.name = os.path.basename(file.name)
        photo.thumbnail = file
        photo.save()
        os.remove(photo_path)
    except photo.DoesNotExist:
        pass


def thumb(url, pk, size):
    chain = download_photo.s(url, size) | update_photo.s(pk)
    chain()
