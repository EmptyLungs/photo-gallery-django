from django.conf.urls import url
from django.conf.urls import include


from . import views


urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'edit/(?P<pk>[0-9]+)/$', views.EditPhoto.as_view(), name='edit'),
    url(r'add', views.AddPhoto.as_view(success_url='/'), name='photo-add'),
    url(r'delete/(?P<pk>\d+)/$', views.DeletePhoto.as_view(), name='delete'),
]
