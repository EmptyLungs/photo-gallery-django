#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Photo


class PhotoAdmin(admin.ModelAdmin):
    'Параметры консоли администратора для модели Photo.'

admin.site.register(Photo, PhotoAdmin)
